# Exercice NLP

Ce dépôt propose un exercice ouvert de traitement du langage naturel (NLP), à partir d'un problème fictif sur les publications de la HAS.

## Problématique métier (FICTIVE !)

Les publications de la HAS sont catégorisées selon divers thématiques, pour permettre la navigation sur le site internet.

Cette catégorisation prend beaucoup de temps aux documentalistes, qui aimeraient automatiser cette tache, en particulier pour les catégorie de la thématique `Maladies et états de santé` qui sont les plus difficiles.

🚀 L'équipe data a proposé d'étudier une fonctionnalité d'assistance à la catégorisation, qui serait intégrée à l'interface d'administration du site.

## Exercice

L'exercice consiste à travailler sur cette fonctionnalité d'assistance fictive.

Différentes étapes pourront être développées :
- Décrire la démarche de travail
- Acquisition et nettoyage des données
- Définition de métrique de succès
- Entraînement de modèles 
- Restitution des résultats
- Description du fonctionnement et de l'architecture de la solution envisagée en production

## En pratique

Une documentation sur les données et catégories est disponible dans le dossier `documentation`.

L'exercice sera développé sur un clone personnel de ce dépôt.

- Les documents descriptifs seront rédigé au format Markdown.
- Le principal langage à utiliser est Python pour le traitement de données. Les codes seront versionnés dans le dépôt (librairie `.py` et/ou notebook `.ipynb`).
- Les résultats pourront être présentés dans des notebooks, ou autre format de restitution au choix.
- La gestion des données utilisées et modèles est laissée libre.

## Disclaimer

Ce problème est potentiellement difficile et chronophage. 

Nous **n'attendons pas** de solution complète ou très performante.

Il sera bienvenu de simplifier le problème, pour s'attacher à un sous-problème plus simple.

Nous nous intéresserons à la démarche générale, aux compétences techniques et scientifiques sur le traitements de données et l'usage de librairies de NLP.
